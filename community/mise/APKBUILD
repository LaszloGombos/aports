# Maintainer: Jeff Dickey <alpine@mise.jdx.dev>
pkgname=mise
pkgver=2024.4.0
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://mise.jdx.dev"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
subpackages="$pkgname-doc"
provides="rtx=$pkgver-r$pkgrel"
replaces="rtx"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdx/mise/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin mise
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mise -t "$pkgdir/usr/bin/"
	install -Dm644 README.md docs/*.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm644 "man/man1/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
eb37ce86be291ce5ea53b61cc7b315d60c44cea805cebbc9f9bd7e8f9fb786a73a22f310ed3a4e69cdd3f6a1c2d40dfc30eddd3ea10a628cda9eb38f9e3add41  mise-2024.4.0.tar.gz
"
